PLANIFICACIÓN DE ATENCIONES MEDICAS

La creación de este software se enfoca en la administracion de atenciones medicas para facilitar la búsqueda de la informacion de cada paciente que se atiende en este recinto.

    • EMPEZANDO

Una vez ejecutado el programa se abrirá una ventana,en la cual tendrá dos opciones a elegir las cuales son medico o secretaria, si es la primera que es utilizado este software, debe elegir la opción de secretaria en la cual tiene que iniciar sesión con la cuenta administrador(Rut = 1, Clave = 1).
En la cuenta administrador tiene privilegios de secretaria, el cual le da la opción de administrar las siguientes opciones:

 - Administrar pacientes
 - Administrar atenciones
 - Administrar previsiones
 - Administrar etiqueta sexo
 - Administrar medicamentos
 - Administrar médicos
 - Administrar secretaria/o
 - Actualizar ventana
 
Dentro de cada opción tendrá 3 opciones las cuales son añadir, editar y eliminar.
La cuenta administrador puede eliminar a cualquier secretaria, pero las secretaria que también pueden manejar esta ventana, no pueden eliminar la cuenta administrador.

La secretaria dispondrá de análisis grafico de datos en el cual uno de ellos corresponde a la cantidad de pacientes de cada sexo, y el otro grafico corresponde a los pacientes que fueron atendidos o no atendidos.

Para poder iniciar sesión en medico, la secretaria debe crear una cuenta de medico, cuando esta cuenta sea creada el medico podrá ingresar a su cuenta y visualizar las atenciones vinculadas a un paciente. Cabe destacar por mucho pacientes que estén ingresados en la base de datos, no se podrán visualizar hasta que la secretaria les haya dado una atención.

El medico podrá atender las atenciones de los pacientes donde podrá agregar a la ficha medica del paciente, un tratamiento, un diagnostico, duración del tratamiento, medicamentos, una enfermedad.

Un medico puede gestionar las enfermedades y visualizar las fichas medicas de cada paciente registradas en la base de datos.

--ESPECIFICACIONES PARA USO DE CODIGO Y PROGRAMA 

Crear base de datos encontrada en dump.
En el archivo database.py se necesita cambiar la contraseña del código a modo personal en la siguiente linea: 
	
	user="root", passwd="CONTRASEÑA MYSQL", database="proyecto")

de esta forma se permitirá la entrada a la base de datos que debe extraerse en el paso anterior. 


    • REQUISITOS PREVIOS
      
Sistema operativo Linux
MySQL - Workbench 
Python 3
Matplotlib
Glade/Gtk/Gnome

    • INSTALACIÓN
      
- Para poder ejecutar el programa debemos obtener python 3 el cual se instala con el siguiente comando: 
	
	sudo apt-get install python3 

- Para el uso de MySQL - Workbench debemos ingresar los comandos: 
	
	sudo apt update
	sudo apt install mysql-server
	sudo mysql_secure_installation

  actualizamos el sistema: 

	$ sudo apt-get update

  instalar el paquete mysql-workbench: 
	
	$ sudo apt-get install mysql-workbench

  Si se nos presentan problemas de depencias, ejecutar el siguiente comando:

	$ sudo apt-get install -f

- Realizamos el código en geany, el cual se instala con el siguiente comando:

	sudo apt-get install geany

- Instalación de glade con el comando: 

	apt install glade

Mientras que para la utilizacion de Gtk en python en nuestro entorno virtual debemos instalar el paquete PyGObject utilizando el siguiente comando: pip install wheel PyGObject.
Luego, para comprobar que la instalacion es correcta podemos hacer uso del siguiente comando: python3 -c 'import gi'. Si esto no arroja ningun tipo de error, quiere decir que la instalacion ha sido exitosa, de no ser asi se debera repetir la instalacion. 


-Creacion del entorno virtual:

    -Instalacion del paquete pip para crear el entorno
    
	sudo apt-get install python3-pip
	
    -Insatalacion del entorno virtual

	sudo pip3 install virtualenv

    -Esto creara una nueva carpeta dentro del directorio que hayamos escogido, allí se
     instalaran todos los paquetes que desees utilizar.	
    
	virtualenv nombre_de_tu_entorno -p python3

    - Para activar el entorno virtual se debe ejecutar este comando

	source nombre_entorno_virtual/bin/activate
    
    -Para desactivar el entorno virtual
    
	deactivate

--EJECUTANDO LAS PRUEBAS POR TERMINAL
 
para ejecutarlo debemos colocar el comando python3 main.py 


    • CONSTRUIDO CON:
      
- Ubuntu: sistema operativo.

- Python: lenguaje de programación utilizado para el código del programa.

- geany: editor de texto para realziar el proyecto

- Glade: herramienta de desarollo visual de interfacez gráficas mediante Gtk/gnome.

- MySQL - Workbench: MySQL Workbench es una herramienta visual de diseño de bases de datos que integra desarrollo de software, administración de bases de datos, diseño de bases de datos, gestión y mantenimiento para el sistema de base de datos MySQL.

- Matplotlib: Matplotlib es una biblioteca para la generación de gráficos a partir de datos contenidos en listas o arrays en el lenguaje de programación Python y su extensión matemática NumPy.

    • VERSIONES
      
Ubuntu 18.04
python 3.6
Glade 3.22.1 / Gnome 3.28.2 
MySQL 8.0.22 / 5.7.32
Workbench 6.3

    • AUTORES

Bryan Ahumada. – Trabajo inicial
Benjamín Astudillo. – Trabajo inicial


    • EXPRESIONES DE GRATITUD

Ejemplos otorgados por Alejandro Valdés Jiménez:
Desde el repositorio -> https://gitlab.com/gtk3/gtk3-samples.git con link directo para clonar. 


