#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import gi
import time
import datetime
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from BaseDatos import Database
from ventana_clientes import Clientes
from utils import Mensage


import matplotlib.pyplot as plt



# Función retorna el valor seleccionado del combo box
def seleccion_combo_box(combo):
    tree_iter = combo.get_active_iter()
    if tree_iter is not None:
        model = combo.get_model()
        seleccion = model[tree_iter][0]
    return seleccion
    
class ViewPacientesParaAtenciones:

    def __init__(self):	
		
        self.builder = Gtk.Builder()
        self.builder.add_from_file("asd/FakeTreeView.glade")
        
        # ventana
        self.viewPacientesAtenciones = self.builder.get_object("creacion_atenciones")
        
        # Botones de administrar.
        self.boton_si = self.builder.get_object("si_seleccion")		
        self.boton_cerrar = self.builder.get_object("salir_seleccion")		
       
        # Eventos
        self.boton_si.connect("clicked", self.boton_click_si)  
        self.boton_cerrar.connect("clicked", self.boton_click_cerrar)  
        
        # TreeView
        self.vista = self.builder.get_object("TreeView_para_atenciones")
        self.actualizar_pacientes_treeView()
        self.viewPacientesAtenciones.show_all()
        
    def boton_click_si(self, btn = None):
        seleccion = self.vista.get_selection()
        model, treeiter = seleccion.get_selected()
        aux = model[treeiter][2]
        aux = str(aux)
        
        if aux:
            uwu = atencion("1", aux, 0)		
		
    def boton_click_cerrar(self, btn = None):
        self.viewPacientesAtenciones.destroy()
        xd = ventana_secretaria(2)

    def actualizar_pacientes_treeView(self, btn = None):
        for col in self.vista.get_columns():
            self.vista.remove_column(col)
        # Base de datos
        x = Database()
	    # Doy valor a la columna
        lista = Gtk.ListStore(str, str, int, str)
        # Consulta para obtener los pacientes.
        sql = """
	    select ficha_cliente.nombre, apellido, rut, prevision.nombre
	    from ficha_cliente inner join prevision on prevision.id_prevision = ficha_cliente.prevision_id_prevision;
	    """        
        asd = x.run_select(sql)
        
	    # agregar elementos al TreeView

        if asd:
            for r in asd:
                lista.append([r[0], r[1], r[2], r[3]])
        
        # titulos de la columna
        
        render = Gtk.CellRendererText()

        # columnas
        columnaNombre = Gtk.TreeViewColumn("Nombre",render,text = 0)
        columnaApellido = Gtk.TreeViewColumn("Apellido",render,text = 1)
        columnaRut = Gtk.TreeViewColumn("Rut",render,text = 2)
        columnaPrevision = Gtk.TreeViewColumn("Previsión",render,text = 3)

        # se agragan datos a las columna
        self.vista.set_model(lista)
        self.vista.append_column(columnaNombre)
        self.vista.append_column(columnaApellido)
        self.vista.append_column(columnaRut)
        self.vista.append_column(columnaPrevision)
        self.vista.show()

    
class atencion:
    
    def __init__(self, rut, nombre_paciente, numero):
        self.rut_secre = rut
        self.nombre_paciente = nombre_paciente
        self.builder = Gtk.Builder()
        self.builder.add_from_file("asd/Mipequeñoglade.glade")
        
        self.ventana_atencion = self.builder.get_object("agregar_atencion")
        # botones
        self.boton_cancelar_atencion = self.builder.get_object("cancelar_atender")
        self.boton_guardar_atencion = self.builder.get_object("guardar_atender")
	
        # eventos
        self.boton_cancelar_atencion.connect("clicked", self.boton_click_cancelar_atencion)  
        self.boton_guardar_atencion.connect("clicked", self.boton_click_guardar_atencion) 
        
        # label 
        self.label_nombre_secre = self.builder.get_object("nombre_secre2")
        self.label_nombre_paciente = self.builder.get_object("nombre_paciente2")
        self.labeladver = self.builder.get_object("adver")
        # asignacion de label

        self.label_nombre_secre.set_text(self.rut_secre)
        self.label_nombre_paciente.set_text(str(nombre_paciente))
	
	    # ~ "Año": fecha[0],
         # ~ "Mes": fecha[1],
          # ~ "Dia": fecha[2],

	    # calendario
        self.calendario = self.builder.get_object("calendario")
        #base dato
        self.db = Database()	
	
	
	    # combo 1
	    # combo box
        self.cmbbox = self.builder.get_object("combo1")
        
	    # combo 2
        self.cmbbox2 = self.builder.get_object("combo2")
	
        renderer = Gtk.CellRendererText()
        self.cmbbox.pack_start(renderer, True)
        self.cmbbox.add_attribute(renderer, "text", 0)
        self.cmbbox2.pack_start(renderer, True)
        self.cmbbox2.add_attribute(renderer, "text", 0)

        self.store =  Gtk.ListStore(str)
        self.store2 =  Gtk.ListStore(str)
        # ~ self.store.append(["08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "00"  ])
        # ~ self.store.append(["00", "15", "30", "45"])
        
        # ARREGLAR
        self.store.append(["08"])     			    
        self.store.append(["09"])     			    
        self.store.append(["10"])     			    
        self.store.append(["11"])     			    
        self.store.append(["12"])     			    
        self.store.append(["13"])     			    
        self.store.append(["14"])     			    
        self.store.append(["15"])     			    
        self.store.append(["16"])     			    
        self.store.append(["17"])     			    
        self.store.append(["18"])     			    
        self.store.append(["19"])     			    
        self.store.append(["20"])     			    
        self.store.append(["21"])     			    
        self.store.append(["22"])     			    
        self.store.append(["23"])     			    
        self.store.append(["00"])     			    
        
        self.store2.append(["00"])     			    
        self.store2.append(["15"])     			    
        self.store2.append(["30"])     			    
        self.store2.append(["45"])
        
        # ARREGLAR	    
        self.cmbbox.set_model(self.store)
        self.cmbbox2.set_model(self.store2)
        self.cmbbox.set_active(0)
        self.cmbbox2.set_active(0)
	    # editar
        self.numero = numero		
        self.ventana_atencion.show_all() 
	
    def boton_click_cancelar_atencion(self, btn = None):
        # cierra ventana.
        self.ventana_atencion.destroy()
        self.ventana_secretaria(2)
        
    def boton_click_guardar_atencion(self, btn = None):
        # Base de datos
        x = Database()

        fecha = self.calendario.get_date()
        kampai = (str(fecha[0]) +"-"+str(fecha[1] + 1) +"-"+ str(fecha[2]))

        # verificar el dia de hoy y la hora.
        sql = """
            select curdate()
        """
        #	
        asd = x.run_select(sql)	  
        j = asd[0]
        asd = j[0]

        fecha_actual = datetime.datetime.strptime(str(asd), '%Y-%m-%d')
        fecha_elegida = datetime.datetime.strptime(kampai, '%Y-%m-%d')
        
        # obtener el valor del combobox#
        combohora = seleccion_combo_box(self.cmbbox)
        combominuto = seleccion_combo_box(self.cmbbox2)
        # #
        
        horaRial = combohora +":"+combominuto
        
        if fecha_elegida >= fecha_actual:
            if self.numero == 1:
                sql = """
                    update atencion set fecha = %(fecha)s, hora = %(hora)s where id_atencion = %(nombre_1)s
	                 """      
                dsa = x.run_sql(sql, {'fecha': fecha_elegida, 'hora': horaRial, 'nombre_1': int(self.nombre_paciente)})     
                uwu = ventana_secretaria(2)        				    				
            else: 
                #
			    # SELECT DATE_FORMAT(NOW( ), "%H:%i" );
		 	    #
                # FALSE = NO ATENDIDO
                # TRUE = ATENDIDO
		 	    
                sql = """
	                 insert into atencion (fecha, Medico_rut, secretaria_rut, ficha_cliente_rut, hora, estado_atencion) values
	                 (%(fecha)s, Null, %(secretaria_rut)s, %(ficha_cliente_rut)s, %(hora)s, False)
	                 """      
                dsa = x.run_sql(sql, {'fecha': fecha_elegida, 'secretaria_rut': self.rut_secre, 'ficha_cliente_rut': (self.nombre_paciente),
                'hora': horaRial})             				    				
            self.ventana_atencion.destroy()
        else:
            dialog = Gtk.MessageDialog(parent=self.ventana_cliente, flags=0, message_type=Gtk.MessageType.QUESTION, 
                buttons=Gtk.ButtonsType.YES_NO, text="UPS ...")
            dialog.format_secondary_text("NO PUEDE CREAR UNA ATENCIÓN DE UN DÍA PASADO.")

            response = dialog.run()
                
            dialog.destroy()

    
class agregar_paciente:
	
    def __init__(self, asd, accion):
        self.zapato = asd
        self.accion = accion
        self.builder = Gtk.Builder()
        self.builder.add_from_file("asd/Mipequeñoglade.glade")
        
        self.ventana_agregar_paciente = self.builder.get_object("registro_paciente")
        
        # botones
        self.boton_guardar = self.builder.get_object("guardar")
        self.boton_cerrar = self.builder.get_object("cerrar")
        
        # eventos
        self.boton_guardar.connect("clicked", self.boton_click_guardar)  
        self.boton_cerrar.connect("clicked", self.boton_click_cerrar) 
        
        # GTK ENTRY
        self.nombre = self.builder.get_object("nombreE")
        self.apellido = self.builder.get_object("apellidoE")
        self.rut = self.builder.get_object("rutE")
        
        # label
        self.label_aviso = self.builder.get_object("labeel")
        # Combo box prevision
        
        self.cmbbox = self.builder.get_object("prevision_box")
        
	# Base de datos
        x = Database()
	
        # RELLENAR COMBO
        renderer = Gtk.CellRendererText()
        self.cmbbox.pack_start(renderer, True)
        self.cmbbox.add_attribute(renderer, "text", 0)
       
        self.store1 = Gtk.ListStore(str)
	
	# Relleno el combo box con el contenido de la tabla prevision
	
        sql = """
            select nombre from prevision;
            """                    
        asd = x.run_select(sql)
	
        self.store1.append(["Seleccione"])
        if asd:
            for r in asd:
                self.store1.append([r[0]])
       
        self.cmbbox.set_model(self.store1)
        self.cmbbox.set_active(0)
        
        # Combo box sexo 
        self.cmbboxSexo = self.builder.get_object("sexo_box")
        
        # RELLENAR COMBO
        # Consulta para obtener el contenido de la tabla sexo y rellenar el combo box
        sql = """
                select nombre from sexo;
                """        
            
        asd = x.run_select(sql)
        
        # agregar elementos al comboBox

        renderer = Gtk.CellRendererText()
        self.cmbboxSexo.pack_start(renderer, True)
        self.cmbboxSexo.add_attribute(renderer, "text", 0)
       
        self.store = Gtk.ListStore(str)
        self.store.append(["Seleccione"])
        if asd:
            for r in asd:
                self.store.append([r[0]])
       
        self.cmbboxSexo.set_model(self.store)
        self.cmbboxSexo.set_active(0)
	
        if self.accion == True:
            
	    # se obtienen los valores del select de Treeview
            frutilla = self.zapato[0]
            self.tigre = (frutilla[2])
            self.nombre.set_text(frutilla[0])
            self.apellido.set_text(frutilla[1])
            self.rut.set_text(str(self.tigre)) 
            
            # Cambiar valor del comboBox
            # combo sexo
            sql = """select sexo.nombre from ficha_cliente inner join sexo on sexo.id_sexo =
                  ficha_cliente.sexo_id_sexo where rut = %(rut)s;
                """
            asd = x.run_select_filter(sql, {'rut': self.tigre})
            id_sexo = asd[0][0]
            if id_sexo:
               index = 0
               for ite in self.store:
                   if(ite[0] == id_sexo):
                       self.cmbboxSexo.set_active(index)
                       break
                   index += 1
            else:
                self.cmbboxSexo.set_active(0)
                
            # combo previsión
            sql = """select prevision.nombre from ficha_cliente inner join prevision on prevision.id_prevision =
                  ficha_cliente.prevision_id_prevision where rut = %(rut)s;
                """
            asd = x.run_select_filter(sql, {'rut': self.tigre})
            id_pre = asd[0][0]
            
            if id_pre:
               index = 0
               for ite in self.store1:
                   if(ite[0] == id_pre):
                       self.cmbbox.set_active(index)
                       break
                   index += 1
            else:
                self.cmbbox.set_active(0)    
            
        self.ventana_agregar_paciente.show_all() 
    
    def boton_click_cerrar(self, btn = None):
        # cierra ventana.
        self.ventana_agregar_paciente.destroy()
        xddd = ventana_secretaria(1)
        
    def boton_click_guardar(self, btn = None):
		# obtener valores combo
        b = seleccion_combo_box(self.cmbbox)
        z = seleccion_combo_box(self.cmbboxSexo)
        
        # obtener valores GTK entry
        nombre_aux = self.nombre.get_text()
        apellido_aux = self.apellido.get_text()
        rut_aux = self.rut.get_text()
    
        if str(b) != 'Seleccione' and str(z) != 'Seleccione':
            if len(nombre_aux) > 2 and len(apellido_aux) > 2:
		# se aceptan
                try:
                    # valido el rut			
                    auux = int(rut_aux)
                    db = Database()
		    
		            # Obtengo el id de sexo
                    sql = """
                        select id_sexo from sexo where nombre = %(nombre)s
                        """        
                    valor_id_sexo = db.run_select_filter(sql, {'nombre': z})
                    aux = valor_id_sexo[0]	
                    valor_id_sexo = aux[0]

		            # Obtengo el id de prevision
                    sql = """
		                select id_prevision from prevision where nombre = %(nombre)s
		                """        
                    valor_id_prevision = db.run_select_filter(sql, {'nombre': b})
                    aux = valor_id_prevision[0]	
                    valor_id_prevision = aux[0]
		    
                    if self.accion == True:
                        sql = """ 
			                update ficha_cliente set nombre = %(nombre)s, apellido = %(apellido)s, 
			                prevision_id_prevision = %(prevision_id_prevision)s, rut = %(rut)s, sexo_id_sexo = %(sexo_id_sexo)s 
			                where rut = %(rut_id_rut)s
			                """
                        db.run_sql(sql, {"nombre": nombre_aux, "apellido": apellido_aux, "prevision_id_prevision": valor_id_prevision,
			            "rut": rut_aux, "sexo_id_sexo": valor_id_sexo, "rut_id_rut": self.tigre})
                    else:
                        sql = """
                            insert into ficha_cliente (rut, prevision_id_prevision, sexo_id_sexo, nombre, apellido) values
			                (%(rut)s, %(prevision_id_prevision)s, %(sexo_id_sexo)s, %(nombre)s, %(apellido)s);
                            """        
                        # dsa = tupla con toda la primera busqueda
                        dsa = db.run_sql(sql, {'rut': rut_aux,'prevision_id_prevision': valor_id_prevision,
			            'sexo_id_sexo': valor_id_sexo ,
		                'nombre': nombre_aux, 'apellido': apellido_aux})    
		    
                    self.nombre.set_text("  ")
                    self.apellido.set_text(" ")
                    self.rut.set_text(" ")
                    self.ventana_agregar_paciente.destroy()
                    xddd = ventana_secretaria(1)
                    
                except ValueError:
                    aux = "Rut Inválido."
                    self.label_aviso.set_text(aux)		    
            else:
                aux = "Ingrese NOMBRE y APELLIDOS Válidos."
                self.label_aviso.set_text(aux)	
        else:
            aux = "Debe Seleccionar"
            self.label_aviso.set_text(aux)	
            

class ventana_agregar_prevision:
	
    def __init__(self):
    
        self.builder = Gtk.Builder()
        self.builder.add_from_file("asd/Mipequeñoglade.glade")
        
        self.ventana_agregar_prevision = self.builder.get_object("ventana_agregar_prevision")
        
	    # botones
        self.boton_cerrar_asd = self.builder.get_object("boton_c")
        self.boton_guardar = self.builder.get_object("botonguardar1")

        # eventos
        self.boton_cerrar_asd.connect("clicked", self.boton_click_cerrar_asd)  
        self.boton_guardar.connect("clicked", self.boton_click_guardar) 

	    # GTK ENTRY
        self.nombre = self.builder.get_object("nombreE2")
        self.ventana_agregar_prevision.show_all() 
	
        # labeel
        self.label_warning = self.builder.get_object("labeel2")

    def boton_click_cerrar_asd(self, btn = None):
        # cierra ventana.
        self.ventana_agregar_prevision.destroy()
        v = ventana_secretaria(3)

        
    def boton_click_guardar(self, btn = None):
        nombre = self.nombre.get_text()
        if len(nombre) > 2:
            db = Database()
            sql = """
                insert into prevision (nombre) values (%(nombre)s)
                """      
            dsa = db.run_sql(sql, {'nombre': nombre})
            
            # CREAR LA VISTA DE ESA PREVISION
            sql = """
                select id_prevision from prevision where nombre = %(nombre)s;
                """ 
            asd = db.run_select_filter(sql, {'nombre': nombre}) 
        
            convenio = asd[0][0]
            # FALSE = NO ATENDIDO
            # TRUE = ATENDIDO
            sql = """
                CREATE VIEW vista_paciente%(x)s as select ficha_cliente.nombre, apellido, rut, prevision.nombre as
                prevision , sexo.nombre as sexo, hora, fecha, atencion.id_atencion from ficha_cliente inner join prevision on  
                prevision.id_prevision = ficha_cliente.prevision_id_prevision inner join sexo on sexo.id_sexo = ficha_cliente.sexo_id_sexo 
                inner join atencion on atencion.ficha_cliente_rut = ficha_cliente.rut where estado_atencion = False and prevision_id_prevision
                = %(xx)s;
            """        
            
            db.run_select_filter(sql, {'x': convenio, 'xx': convenio })
            
            self.ventana_agregar_prevision.destroy()
            v = ventana_secretaria(3)
        else:    
            self.label_warning.set_text("Debe ingresar un texto válido.")


class ventana_agregar_sexo:
	
    def __init__(self, tabla):
        self.tabla = tabla      
        self.builder = Gtk.Builder()
        self.builder.add_from_file("asd/Mipequeñoglade.glade")
        
        self.ventana_agregar_sexo = self.builder.get_object("ventana_agregar_sexo")
        
	    # botones
        self.boton_cerrar = self.builder.get_object("boton_cerr")
        self.boton_guardar = self.builder.get_object("botonguardar")


        # eventos
        self.boton_cerrar.connect("clicked", self.boton_click_cerrar)  
        self.boton_guardar.connect("clicked", self.boton_click_guardar) 

	    # GTK ENTRY
        self.nombre = self.builder.get_object("nombreE1")
        self.ventana_agregar_sexo.show_all() 
	
        # labeel
        self.label_warning = self.builder.get_object("labeel1")

    def boton_click_cerrar(self, btn = None):
        # cierra ventana.
        self.ventana_agregar_sexo.destroy()
        if self.tabla == "sexo":
            v = ventana_secretaria(4)
        if self.tabla == "Medicamentos":
            v = ventana_secretaria(5)			

    def boton_click_guardar(self, btn = None):
        nombre = self.nombre.get_text()
        if len(nombre) > 2:
            if self.tabla == "sexo":
                db = Database()
                sql = """
                    insert into sexo (nombre) values (%(nombre)s)
                    """      
                dsa = db.run_sql(sql, {'nombre': nombre})
                self.ventana_agregar_sexo.destroy()
                v = ventana_secretaria(4)
            if self.tabla == "Medicamentos":
                db = Database()
                sql = """
                    insert into Medicamentos (nombre) values (%(nombre)s)
                    """      
                dsa = db.run_sql(sql, {'nombre': nombre})
                self.ventana_agregar_sexo.destroy()
                v = ventana_secretaria(5)
                				    
        else:    
            self.label_warning.set_text("Debe ingresar un texto válido.")
	
    
class ventana_editar_sexo_prevision:
	
    def __init__(self, aux, tabla):
        self.aux = aux
        self.tabla = tabla    
        self.builder = Gtk.Builder()
        self.builder.add_from_file("asd/Mipequeñoglade.glade")
        
        self.win_editar_sexo_prevision = self.builder.get_object("win_editar_sexo_prevision")
	
	    # Gtk entry	
        self.entry_editable = self.builder.get_object("entry_edditar")
        self.entry_editable.set_text(self.aux)
	
	    # botones
        self.boton_editar = self.builder.get_object("ediiiii")
        self.cerrar_editar = self.builder.get_object("cerrar_editar")
	
	    # label 
        self.labbel_editar = self.builder.get_object("labbel_editar")
        
	    # base de datos
        self.db = Database()
	
	    # eventos 
        self.cerrar_editar.connect("clicked", self.click_cerrar_registro_editar)
        self.boton_editar.connect("clicked", self.click_boton_editar)
	
        self.win_editar_sexo_prevision.show_all()
	
    def click_cerrar_registro_editar(self, btn = None):
        self.win_editar_sexo_prevision.destroy()
        if (self.tabla == "prevision"):
               
            v = ventana_secretaria(3)

        if (self.tabla == "sexo"):
               
            v = ventana_secretaria(4)
                
        if (self.tabla == "Medicamentos"):
              
            v = ventana_secretaria(5)          
	
    def click_boton_editar(self, btn = None):
        manzana = self.entry_editable.get_text()
        if (len(manzana) == 0):
            print ("warning")
        else:
            if (self.tabla == "prevision"):
				# renombrar view de la base RENAME TABLE hola to xd;
                # consulta
                sql = """
                    update prevision set nombre = %(nombre)s where nombre = %(nombre_1)s
                """
                self.db.run_sql(sql, {"nombre": manzana, "nombre_1": self.aux})
                self.win_editar_sexo_prevision.destroy()
                v = ventana_secretaria(3)

            if (self.tabla == "sexo"):
                # consulta
                sql = """
	                update sexo set nombre = %(nombre)s where nombre = %(nombre_1)s
		        """
                self.db.run_sql(sql, {"nombre": manzana, "nombre_1": self.aux})
                self.win_editar_sexo_prevision.destroy()
                v = ventana_secretaria(4)
                
            if (self.tabla == "Medicamentos"):
                # consulta
                sql = """
	                update Medicamentos set nombre = %(nombre)s where nombre = %(nombre_1)s
		        """
                self.db.run_sql(sql, {"nombre": manzana, "nombre_1": self.aux})
                self.win_editar_sexo_prevision.destroy()
                v = ventana_secretaria(5)                
                
class ventana_registro_medico:
	
    def __init__(self, rut, zapato):
        self.rute = rut
        self.zapato = zapato
        self.builder = Gtk.Builder()
        self.builder.add_from_file("asd/Mipequeñoglade.glade")
        
        self.ventana_registro_medico = self.builder.get_object("registro_medico")
        
        # botones
        self.boton_guardar = self.builder.get_object("boton_guarda")
        self.boton_cerrar = self.builder.get_object("boton_cerra")

        
        # eventos
        self.boton_guardar.connect("clicked", self.boton_click_guardar)  
        self.boton_cerrar.connect("clicked", self.boton_click_cerrar) 

        # GTK ENTRY
        self.nombre = self.builder.get_object("nombre_entry")
        self.apellido = self.builder.get_object("apellido_entry")
        self.rut = self.builder.get_object("rut_entry")
        self.clave = self.builder.get_object("clave_entry")
        
        # combo box
        self.cmbbox_equipos = self.builder.get_object("convenio_box")
        
        # RELLENAR COMBO

	    # Base de datos
        x = Database()
       
        renderer = Gtk.CellRendererText()
        self.cmbbox_equipos.pack_start(renderer, True)
        self.cmbbox_equipos.add_attribute(renderer, "text", 0)
        
        sql = """
            select nombre from prevision;
            """                    
        asd = x.run_select(sql)
       
        self.store = Gtk.ListStore(str)
        self.store.append(["Seleccione"])
        if asd:
            for r in asd:
                self.store.append([r[0]])
      
        self.cmbbox_equipos.set_model(self.store)
        self.cmbbox_equipos.set_active(0)
   
        # label 
        self.label_aviso = self.builder.get_object("sad")
        
        if zapato == True:

            # guardar			
            db = Database()
            sql = """
                select rut, nombre, apellido, clave from Medico where rut = %(nombre)s
                """                           
            xdxd = db.run_select_filter(sql, {'nombre': self.rute})
            
            self.nombre.set_text(str(xdxd[0][1]))
            self.apellido.set_text(str(xdxd[0][2]))
            self.rut.set_text(str(xdxd[0][0]))
            self.clave.set_text(str(xdxd[0][3]))
        
        self.ventana_registro_medico.show_all() 
        
        
    # llama una ventana de dialogo, con interaccion con el usuario.          

    def boton_click_cerrar(self, btn = None):
        self.ventana_registro_medico.destroy()
        v = ventana_secretaria(7)

    def boton_click_guardar(self, btn = None):
        b = seleccion_combo_box(self.cmbbox_equipos)
        nombre_aux = self.nombre.get_text()
        apellido_aux = self.apellido.get_text()
        rut_aux = self.rut.get_text()
        clave_aux  = self.clave.get_text()
        
        if str(b) != 'Seleccione':	
            if len(nombre_aux) > 2 and len(apellido_aux) > 2:
			    # se aceptan
                try:
                    # valido el rut			
                    auux = int(rut_aux)
                    
                    if len(clave_aux) > 1:		
                        # guardar			
                        # consulta para obtener el id de la prevision elegida.
                        db = Database()
                        sql = """
                                select id_prevision from prevision where nombre = %(nombre)s
                                """        
                        xdxd = db.run_select_filter(sql, {'nombre': b})
                        aux = xdxd[0]
                        xdxd = aux[0]
                        if self.zapato == False:
                            sql = """
                                insert into Medico (rut, nombre, apellido, clave, prevision_id_prevision) values (%(rut)s, %(nombre)s, %(apellido)s, %(clave)s, %(convenio)s);
                                """        
                            # dsa = tupla con toda la primera busqueda
                            dsa = db.run_sql(sql, {'rut': rut_aux, 'nombre': nombre_aux, 'apellido': apellido_aux,
                            'clave': clave_aux, 'convenio': xdxd})
                            self.nombre.set_text("  ")
                            self.apellido.set_text(" ")
                            self.rut.set_text(" ")
                            self.clave.set_text(" ")
                        
                        if self.zapato == True:
                            sql = """
	                        update Medico set rut = %(rut)s, nombre = %(nombre)s, apellido = %(apellido)s,
	                        clave = %(clave)s, prevision_id_prevision = %(prevision)s where rut = %(nombre_1)s
		                    """
                            db.run_sql(sql, {'rut': rut_aux, "nombre": nombre_aux,'apellido': apellido_aux, 'clave': clave_aux,
                            "prevision": xdxd,  "nombre_1": self.rute})
               
                        self.ventana_registro_medico.destroy()	
                        v = ventana_secretaria(6)
                                        	    
                except ValueError:
                    aux = "Rut Inválido."
                    self.label_aviso.set_text(aux)		    
            else:
                aux = "Ingrese NOMBRE y APELLIDOS Válidos."
                self.label_aviso.set_text(aux)	
        
class ventana_anadir_secre:
	
    def __init__(self, rut, zapato):
        self.rute = rut
        self.zapato = zapato
        self.builder = Gtk.Builder()
        self.builder.add_from_file("asd/Mipequeñoglade.glade")
        
        self.ventana_secre = self.builder.get_object("registro_secre")
        # botones
        self.boton_anadir = self.builder.get_object("boton_guardar")
        self.boton_cerrar = self.builder.get_object("boton_cerrar")
               
        # eventos
        self.boton_anadir.connect("clicked", self.boton_click_anadir)  
        self.boton_cerrar.connect("clicked", self.boton_click_cerrar) 

        # GTK ENTRY
        self.nombre = self.builder.get_object("entry_nombre")
        self.apellido = self.builder.get_object("entry_apellido")
        self.rut = self.builder.get_object("entry_rut")
        self.clave = self.builder.get_object("entry_clave")
        
        # label 
        self.label_aviso = self.builder.get_object("label_secre")
        
        if zapato == True:

            # guardar			
            db = Database()
            sql = """
                select rut, nombre, apellido, clave from secretaria where rut = %(nombre)s
                """                           
            xdxd = db.run_select_filter(sql, {'nombre': self.rute})
            
            self.nombre.set_text(str(xdxd[0][1]))
            self.apellido.set_text(str(xdxd[0][2]))
            self.rut.set_text(str(xdxd[0][0]))
            self.clave.set_text(str(xdxd[0][3]))
        
        self.ventana_secre.show_all() 

    def boton_click_anadir(self, btn = None):
		# guardar			
        db = Database()
        if self.zapato == False or self.zapato == True:
            # ads
            nombre_aux = self.nombre.get_text()
            apellido_aux =self.apellido.get_text()
            rut_aux = self.rut.get_text()
            clave_aux  = self.clave.get_text()
        
            if len(nombre_aux) > 2 and len(apellido_aux) > 2:
			    # se aceptan
                try:
                    # valido el rut			
                    auux = int(rut_aux)
                    if len(clave_aux) > 1:		
                        if self.zapato == False:
                            sql = """
                                insert into secretaria (rut, nombre, apellido, clave) values (%(rut)s, %(nombre)s, %(apellido)s, %(clave)s);
                                """        
                             # dsa = tupla con toda la primera busqueda
                            dsa = db.run_sql(sql, {'rut': rut_aux, 'nombre': nombre_aux, 'apellido': apellido_aux, 'clave': clave_aux})
                            self.nombre.set_text("  ")
                            self.apellido.set_text(" ")
                            self.rut.set_text(" ")
                            self.clave.set_text(" ")
                            self.ventana_secre.destroy()
                    
                        if self.zapato == True:
                            sql = """
	                        update secretaria set rut = %(rut)s, nombre = %(nombre)s, apellido = %(apellido)s, clave = %(clave)s where rut = %(nombre_1)s
		                    """
                            db.run_sql(sql, {'rut': rut_aux, "nombre": nombre_aux,'apellido': apellido_aux, 'clave': clave_aux, "nombre_1": self.rute})
                            self.ventana_secre.destroy()
                             
                except ValueError:
                        aux = "Rut Inválido."
                        self.label_aviso.set_text(aux)		    
            else:
                aux = "Ingrese NOMBRE y APELLIDOS Válidos."
                self.label_aviso.set_text(aux)	
            v = ventana_secretaria(7)                
        
    def boton_click_cerrar(self, btn = None):
        self.ventana_secre.destroy()	
        v = ventana_secretaria(7)


class ventana_secretaria:

    def __init__(self, zanahoria):	
		########     self.num	
        ########        Tabla valores           ########
        #         Administrar Pacientes     =  # 1 #   #
        #         Administrar Atenciones    =  # 2 #   #
        #         Administrar Previsiones   =  # 3 #   #
        #         Administrar Sexos         =  # 4 #   #
        #         Administrar Medicamentos  =  # 5 #   #
        #         Administrar Medicos       =  # 6 #   #
        #         Administrar Secretaria    =  # 7 #   #
        ########                                ########
        self.num = zanahoria
        self.builder = Gtk.Builder()
        self.builder.add_from_file("asd/FakeTreeView.glade")
        
        # ventana
        self.ventana_secre = self.builder.get_object("ventana_secretaria")
        
        # Botones de administrar.
        self.boton_AdministrarPacientes = self.builder.get_object("Administrar_pacientes")		
        self.boton_AdministrarAtenciones = self.builder.get_object("administrar_atenciones")		
        self.boton_AdministrarPrevisiones = self.builder.get_object("administrar_previsiones")
        self.boton_AdministrarSexo = self.builder.get_object("administrar_sexo")
        self.boton_AdministrarMedicamentos = self.builder.get_object("administrar_medicamentos")		
        self.boton_AdministrarMedicos = self.builder.get_object("administrar_medicos")
        self.boton_AdministrarSecretaria = self.builder.get_object("administrar_secretaria")
        
        
        self.botonGraficoSexo = self.builder.get_object("grafico_sexo")
        self.botonGraficoAtencion = self.builder.get_object("grafico_atenciones")
        self.botonGraficoPrevision = self.builder.get_object("grafico_pre")
        
        self.boton_Actualizar = self.builder.get_object("act")
        
        # botones que alteran Bd
        self.boton_anadir = self.builder.get_object("anadirrrrr")
        self.boton_editar = self.builder.get_object("editarrrrr")
        self.boton_eliminar = self.builder.get_object("eliminarrrr")
        
        # botones que administran la ventana
        self.boton_cerrar_sesion = self.builder.get_object("cerrar_sesion")
        self.boton_cerrar = self.builder.get_object("cerrar_ventana")		
        # ~ # Número que válida las opciones.
        # ~ self.num = 0;
        
        # ~ # label
        # ~ self.label_nombre = self.builder.get_object("nombre")
        # ~ self.label_sesion = self.builder.get_object("sesion")
        self.label_informativo = self.builder.get_object("label_informativo")
        
        # ~ if self.num != 0:        
        # Los label obtienen valores.
            # ~ self.label_nombre.set_text("Nombre Usuario: " + nombre_secretaria + " " + apellido_secretaria)
            # ~ self.label_sesion.set_text("Sesión Usuario: " + str(rut))
            # ~ self.label_informativo.set_text("Seleccione una opción ADMINISTRATIVA")
        
        # Eventos
        self.boton_cerrar.connect("clicked", self.boton_click_cerrar_ventana)  
        self.boton_cerrar_sesion.connect("clicked", self.boton_click_cerrar_sesion)  
        
        # EVENTOS DE botones que alteran Bd
        self.boton_anadir.connect("clicked", self.boton_click_anadir)  
        self.boton_editar.connect("clicked", self.boton_click_editar)
        self.boton_eliminar.connect("clicked", self.boton_click_eliminar)
        
        # Eventos de botones ADMINISTRAR
        self.boton_AdministrarPacientes.connect("clicked", self.boton_administrarPacientes)  		
        self.boton_AdministrarAtenciones.connect("clicked", self.boton_administrar_atenciones)  		
        self.boton_AdministrarPrevisiones.connect("clicked", self.boton_administrarPrevisiones)  
        self.boton_AdministrarSexo.connect("clicked", self.boton_administrarSexo)  
        self.boton_AdministrarMedicamentos.connect("clicked", self.boton_administrarMedicamentos)  		
        self.boton_AdministrarMedicos.connect("clicked", self.boton_administrarMedicos)  
        self.boton_AdministrarSecretaria.connect("clicked", self.boton_administrarSecretaria)
        
        
        self.botonGraficoSexo.connect("clicked", self.boton_GraficoSexo)
        self.botonGraficoAtencion.connect("clicked", self.boton_GraficoAtenciones)
        self.botonGraficoPrevision.connect("clicked", self.boton_GraficoPre)
        self.boton_Actualizar.connect("clicked", self.boton_actualizar)    
        
        # TreeView
        self.vista = self.builder.get_object("arbolito")
           
        if self.num == 1:
            self.actualizar_pacientes_treeView()			
        if self.num == 2:
            self.actualizar_atenciones_Treeview()			           
        if self.num == 3:
            self.actualizar_treeView_prevision_sexo_medicamentos()	        
        if self.num == 4:
            self.actualizar_treeView_prevision_sexo_medicamentos()	       
        if self.num == 5:
            self.actualizar_treeView_prevision_sexo_medicamentos()	       
        if self.num == 6:
            self.actualizar_medico_secretaria_Treeview()
        if self.num == 7:
            self.actualizar_medico_secretaria_Treeview()			
                                
        self.ventana_secre.show_all()
        
        
        
    def boton_GraficoPre(self, btn = None):
        dialog = Gtk.MessageDialog(parent=self.ventana_secre, flags=0, message_type=Gtk.MessageType.QUESTION, 
            buttons=Gtk.ButtonsType.YES_NO, text="¡CUIDADO!")
        dialog.format_secondary_text("Para una mejor calidad del gráfico, verifique que EXISTAN DATOS EN PREVISIÓN.")
         
        response = dialog.run()
            
        if response == Gtk.ResponseType.YES:      
            x = Database()
            sql = """select count(nombre) from prevision ;
            """
            asd = x.run_select(sql)
       
            numero_sexo = asd[0][0]
        
            sql = """select nombre from prevision;
            """
            tupla_sexo = x.run_select(sql)
            names = []
            values = []
        
            for tupla_sexoo in tupla_sexo:
                sql = """ select count(prevision_id_prevision) from ficha_cliente inner join 
                prevision on prevision.id_prevision = ficha_cliente.prevision_id_prevision
                where prevision.nombre = %(id)s;
                """
            
                asd = x.run_select_filter(sql, {'id': tupla_sexoo[0]})
                names.append(str(tupla_sexoo[0]))
                values.append(asd[0][0])
            
            plt.bar(names, values)        
            plt.ylabel = ("y")
            plt.xlabel= ("x")
            plt.title("Gráfico de Previsión")
            plt.show()            
           
        dialog.destroy()                 		
        
        
    def boton_GraficoSexo(self, btn = None):
        dialog = Gtk.MessageDialog(parent=self.ventana_secre, flags=0, message_type=Gtk.MessageType.QUESTION, 
            buttons=Gtk.ButtonsType.YES_NO, text="¡CUIDADO!")
        dialog.format_secondary_text("Para una mejor calidad del gráfico, verifique que EXISTAN DATOS EN ETIQUETAS DE SEXO.")
         
        response = dialog.run()
            
        if response == Gtk.ResponseType.YES:      
            x = Database()
            sql = """select count(nombre) from sexo ;
            """
            asd = x.run_select(sql)
       
            numero_sexo = asd[0][0]
        
            sql = """select nombre from sexo;
            """
            tupla_sexo = x.run_select(sql)
            names = []
            values = []
        
            for tupla_sexoo in tupla_sexo:
                sql = """ select count(sexo_id_Sexo) from ficha_cliente inner join sexo on sexo.id_sexo = ficha_cliente.sexo_id_sexo
                where sexo.nombre = %(id)s;
                """
            
                asd = x.run_select_filter(sql, {'id': tupla_sexoo[0]})
                names.append(str(tupla_sexoo[0]))
                values.append(asd[0][0])
            
            plt.bar(names, values)        
            plt.ylabel = ("y")
            plt.xlabel= ("x")
            plt.title("Gráfico de Sexos")
            plt.show()            
           
        dialog.destroy()        
	        
    def boton_GraficoAtenciones(self, btn = None):
        dialog = Gtk.MessageDialog(parent=self.ventana_secre, flags=0, message_type=Gtk.MessageType.QUESTION, 
            buttons=Gtk.ButtonsType.YES_NO, text="¡CUIDADO!")
        dialog.format_secondary_text("Para un gráfico de mejor calidad, recuerde tener datos existentes en ATENCIONES.")
         
        response = dialog.run()
            
        if response == Gtk.ResponseType.YES:      
            names = ['Atendidos', 'No atendidos']
            x = Database()
            sql = """select count(estado_atencion) from atencion where estado_atencion =  False;
            """
            asd = x.run_select(sql)
                   
            valor_true = asd[0][0]
            
            sql = """select count(estado_atencion) from atencion where estado_atencion =  True;
             """
            asd = x.run_select(sql)
        
            valor_false = asd[0][0]
        
            x_C = valor_true
            x_p = valor_false
        
            values = [x_C, x_p]
        
            plt.bar(names, values)
        
            plt.ylabel = ("y")
            plt.xlabel= ("x")
            plt.title("Gráfico de Atenciones")
            plt.show()     
              			
        dialog.destroy()        
         
    def boton_actualizar(self, btn = None):
        self.ventana_secre.destroy()
        ventana_secretaria(0)    
     
    def boton_click_cerrar_ventana(self, btn = None):
        self.ventana_secre.destroy()
        exit()
        
    def boton_click_cerrar_sesion(self, btn = None):
        self.ventana_secre.destroy()
        # le mando inicio sesion SECRETARIA 
        k = Inicio_sesion(2)
        
    def boton_administrarPacientes(self, btn = None):
        self.num = 1
        self.label_informativo.set_text("   ")
        self.ventana_secre.set_title("Pacientes")
        self.actualizar_pacientes_treeView()
        self.ventana_secre.show_all()
        
    def boton_administrar_atenciones(self, btn = None):
        self.label_informativo.set_text("   ")
        self.ventana_secre.set_title("Atenciones")
        self.actualizar_atenciones_Treeview()			
        self.num = 2
        self.ventana_secre.show_all()
        
    def boton_administrarPrevisiones(self, btn = None):
        self.num = 3
        self.label_informativo.set_text("   ")
        self.ventana_secre.set_title("Previsiones")
        self.actualizar_treeView_prevision_sexo_medicamentos()
        self.ventana_secre.show_all()

    def boton_administrarSexo(self, btn = None):
        self.num = 4
        self.label_informativo.set_text("   ")
        self.ventana_secre.set_title("Sexos")
        self.actualizar_treeView_prevision_sexo_medicamentos()
        self.ventana_secre.show_all()
        
    def boton_administrarMedicamentos(self, btn = None):
        self.num = 5
        self.label_informativo.set_text("   ")
        self.ventana_secre.set_title("Medicamentos")
        self.actualizar_treeView_prevision_sexo_medicamentos()
        self.ventana_secre.show_all()
        
    def boton_administrarMedicos(self, btn = None):
        self.num = 6
        self.label_informativo.set_text("   ")
        self.ventana_secre.set_title("Médico")
        self.actualizar_medico_secretaria_Treeview()
        self.ventana_secre.show_all()
        
    def boton_administrarSecretaria(self, btn = None):
        self.num = 7
        self.label_informativo.set_text("   ")
        self.ventana_secre.set_title("Secretaria/o")
        self.actualizar_medico_secretaria_Treeview()
        self.ventana_secre.show_all()
     #
     # Sección EDITAR #####
     #
     #
    def boton_click_editar(self, btn = None):
	    # Debo capturar el valor del TreeView
        x = Database()			
        seleccion = self.vista.get_selection()
        model, treeiter = seleccion.get_selected()
        aux = model[treeiter][0]
        aux = str(aux)

        if (self.num == 0):
            # agregar Ventana Warning
            print ("Seleccione una opcion ADMINISTRAR PRIMERO")	
        # Pacientes
        elif self.num == 1:
            platano = model[treeiter][2]
            sql = """
	            select ficha_cliente.nombre, apellido, rut, prevision.nombre, sexo.nombre
	            from ficha_cliente inner join prevision on prevision.id_prevision = ficha_cliente.prevision_id_prevision 
	            inner join sexo on sexo.id_sexo = ficha_cliente.sexo_id_sexo where rut = %(nombre)s;
	            """        
            asd = x.run_select_filter(sql, {'nombre': platano})
            self.ventana_secre.destroy()
            tusa = agregar_paciente(asd, True)
        # Atenciones
        elif self.num == 2:	
            uwu = atencion("1", aux, 1)	 			
            self.ventana_secre.destroy()
        # Previsiones
        elif self.num == 3:
            ventana_editar_sexo_prevision(str(aux), "prevision")	
            self.ventana_secre.destroy()	
        # Sexos
        elif self.num == 4:
            ventana_editar_sexo_prevision(str(aux), "sexo")
            self.ventana_secre.destroy()	
        # Medicamentos
        elif self.num == 5: 
            ventana_editar_sexo_prevision(str(aux), "Medicamentos")
            self.ventana_secre.destroy()					
        # Medicos
        elif self.num == 6:		
            aw = ventana_registro_medico(str(aux), True)
            self.ventana_secre.destroy()
        # Secretaria
        elif self.num == 7:
            lechuga = int(aux)
            aw = ventana_anadir_secre(lechuga, True)
            self.ventana_secre.destroy()
        # Ninguno de los casos
        else:
            print ("parametro maloooo :c")		
    #
    #
    # seccion eliminar ####
    #
    #
    def boton_click_eliminar(self, btn = None):
        x = Database()
		# Debo capturar el valor del TreeView
        seleccion = self.vista.get_selection()
        model, treeiter = seleccion.get_selected()
        aux = model[treeiter][0]
        aux = str(aux)
		
        if (self.num == 0):
            # agregar Ventana Warning
            print ("Seleccione una opcion ADMINISTRAR PRIMERO")	
        # Pacientes
        elif self.num == 1:
			
            if treeiter is not None:
                dialog = Gtk.MessageDialog(parent=self.ventana_secre, flags=0, message_type=Gtk.MessageType.QUESTION, 
                    buttons=Gtk.ButtonsType.YES_NO, text="Se eliminara a " + model[treeiter][1])
                dialog.format_secondary_text("Esta acción no se puede revertir.")
            
                response = dialog.run()
            
                if response == Gtk.ResponseType.YES:
                    sql = """
                        delete from ficha_cliente where rut = %(nombre)s
                    """
                    x.run_sql(sql, {'nombre': str(model[treeiter][2])})
                    self.actualizar_pacientes_treeView()
                    self.ventana_secre.show_all()
                    
                dialog.destroy()	    
                      
        # Atenciones
        elif self.num == 2:	

            if treeiter is not None:
                dialog = Gtk.MessageDialog(parent=self.ventana_secre, flags=0, message_type=Gtk.MessageType.QUESTION, 
                    buttons=Gtk.ButtonsType.YES_NO, text="Se eliminará la atención de: " + model[treeiter][1])
                dialog.format_secondary_text("Esta acción no se puede revertir.")
            
                response = dialog.run()
            
                if response == Gtk.ResponseType.YES:
                    sql = """
                    delete from atencion where id_atencion = %(nombre)s
                    """
                    x.run_sql(sql, {'nombre': aux})
                    self.actualizar_atenciones_Treeview()
                    self.ventana_secre.show_all()        
                dialog.destroy()	    
                    
        # Previsiones
        elif self.num == 3:
            if treeiter is not None:
                dialog = Gtk.MessageDialog(parent=self.ventana_secre, flags=0, message_type=Gtk.MessageType.QUESTION, 
                    buttons=Gtk.ButtonsType.YES_NO, text="Se eliminara a " + model[treeiter][0])
                dialog.format_secondary_text("Esta acción no se puede revertir.")
            
                response = dialog.run()
            
                if response == Gtk.ResponseType.YES:
                    sql = """
                    select id_prevision from prevision where nombre = %(nombre)s;
                    """ 
                    asd = x.run_select_filter(sql, {'nombre': aux}) 

                    sql = """
                    delete from prevision where nombre = %(nombre)s
                    """
                    x.run_sql(sql, {'nombre': aux})
        
                    convenio = asd[0][0]

                    sql = """
                    DROP VIEW vista_paciente%(x)s
                    """ 
                    x.run_select_filter(sql, {'x': asd[0][0]})
                    self.actualizar_treeView_prevision_sexo_medicamentos()
                    self.ventana_secre.show_all()
                    
                dialog.destroy()	    			
            
        # Sexos
        elif self.num == 4:
            if treeiter is not None:
                dialog = Gtk.MessageDialog(parent=self.ventana_secre, flags=0, message_type=Gtk.MessageType.QUESTION, 
                    buttons=Gtk.ButtonsType.YES_NO, text="Se eliminara a " + model[treeiter][0])
                dialog.format_secondary_text("Esta acción no se puede revertir.")
            
                response = dialog.run()
            
                if response == Gtk.ResponseType.YES:
                    sql = """
                    delete from sexo where nombre = %(nombre)s
                    """
                    x.run_sql(sql, {'nombre': aux})
                    self.actualizar_treeView_prevision_sexo_medicamentos()
                    self.ventana_secre.show_all()                    
                dialog.destroy()				

            # Medicamentos
        elif self.num == 5: 
            if treeiter is not None:
                dialog = Gtk.MessageDialog(parent=self.ventana_secre, flags=0, message_type=Gtk.MessageType.QUESTION, 
                    buttons=Gtk.ButtonsType.YES_NO, text="Se eliminara a " + model[treeiter][0])
                dialog.format_secondary_text("Esta acción no se puede revertir.")
            
                response = dialog.run()
            
                if response == Gtk.ResponseType.YES:
                    sql = """
                    delete from Medicamentos where nombre = %(nombre)s
                    """
                    x.run_sql(sql, {'nombre': aux})
                    self.actualizar_treeView_prevision_sexo_medicamentos()
                    self.ventana_secre.show_all()                    
                dialog.destroy()				

                    
        # Medicos
        elif self.num == 6:
            if treeiter is not None:
                dialog = Gtk.MessageDialog(parent=self.ventana_secre, flags=0, message_type=Gtk.MessageType.QUESTION, 
                    buttons=Gtk.ButtonsType.YES_NO, text="Se eliminara a " + model[treeiter][1])
                dialog.format_secondary_text("Esta acción no se puede revertir.")
            
                response = dialog.run()
            
                if response == Gtk.ResponseType.YES:
                    sql = """
                        delete from Medico where rut = %(nombre)s
                       """					
                    x.run_sql(sql, {'nombre': aux})
                    self.actualizar_medico_secretaria_Treeview()
                    self.ventana_secre.show_all()        
                dialog.destroy()				
            
            
        # Secretaria
        elif self.num == 7:
            if treeiter is not None:
                dialog = Gtk.MessageDialog(parent=self.ventana_secre, flags=0, message_type=Gtk.MessageType.QUESTION, 
                    buttons=Gtk.ButtonsType.YES_NO, text="Se eliminara a " + model[treeiter][1])
                dialog.format_secondary_text("Esta acción no se puede revertir.")
            
                response = dialog.run()
            
                if response == Gtk.ResponseType.YES:
                    sql = """
                    delete from secretaria where rut = %(nombre)s
                    """					
                    x.run_sql(sql, {'nombre': aux})
                    self.actualizar_medico_secretaria_Treeview()
                    self.ventana_secre.show_all()
                    
                dialog.destroy()				
            
        # Ninguno de los casos
        else:
            print ("parametro maloooo :c")		
	#
	#
	# seccion agregar
	#
	#
    def boton_click_anadir(self, btn = None):        
        x = Database()
        if (self.num == 0):
            # agregar Ventana Warning
            print ("Seleccione una opcion ADMINISTRAR PRIMERO")	
        # Pacientes
        elif self.num == 1:
            xd = agregar_paciente(" ", False)
            self.ventana_secre.destroy()
        # Atenciones
        elif self.num == 2:		
            print (self.num)
            xddd = ViewPacientesParaAtenciones()
            self.ventana_secre.destroy()	
        # Previsiones
        elif self.num == 3:
            k = ventana_agregar_prevision()
            self.ventana_secre.destroy()			
        # Sexos
        elif self.num == 4:
            k = ventana_agregar_sexo("sexo")
            self.ventana_secre.destroy()
            
        # Medicamentos
        elif self.num == 5: 
            k = ventana_agregar_sexo("Medicamentos")
            self.ventana_secre.destroy()
            
        # Medicos
        elif self.num == 6:		
            sad = ventana_registro_medico(0, False)
            self.actualizar_medico_secretaria_Treeview()
            self.ventana_secre.hide()
        # Secretaria
        elif self.num == 7:
            aw = ventana_anadir_secre(0, False)
            self.actualizar_medico_secretaria_Treeview()
            self.ventana_secre.hide()
        # Ninguno de los casos
        else:
            print ("parametro maloooo :c")		
    
    def actualizar_medico_secretaria_Treeview(self):
	    
        for col in self.vista.get_columns():
            self.vista.remove_column(col)		     
        lista = Gtk.ListStore(int, str, str, str)
        # Doy valor a la columna
        if (self.num == 6):
            # Base de datos
            x = Database()			
            # Consulta para obtener los pacientes.
            sql = """
                select rut, Medico.nombre, apellido, prevision.nombre from Medico inner join                  
                prevision on prevision.id_prevision = Medico.prevision_id_prevision;

	        """        
            asd = x.run_select(sql)
        
	        # agregar elementos al TreeView
            if asd:
                for r in asd:
                    lista.append([r[0], r[1], r[2], r[3]])
                    
        
        if (self.num == 7):
			#### SE CREA VIEW PARA QUE NO SE PUEDA ELIMINAR EL ADMIN. ####
			# Base de datos
            x = Database() 
            
			# ~ sql = """
                # ~ CREATE VIEW secre_view as select rut, nombre, apellido from secretaria where rut != 1;

            # ~ """        
            
            lista = Gtk.ListStore(int, str, str)    
            
            # Consulta para obtener las secretarias.
            sql = """
                select * from secre_view;
	        """        
            asd = x.run_select(sql)
        
	        # agregar elementos al TreeView
            if asd:
                for r in asd:
                    lista.append([r[0], r[1], r[2]])

        render = Gtk.CellRendererText()

        # columnas
        columnaNombre = Gtk.TreeViewColumn("RUT",render,text = 0)
        columnaApellido = Gtk.TreeViewColumn("Nombre",render,text = 1)
        columnaRut = Gtk.TreeViewColumn("Apellido",render,text = 2)
        
        if (self.num == 6):
            columnaPrevision = Gtk.TreeViewColumn("Convenio",render,text = 3)

        # se agragan datos a las columna
        self.vista.set_model(lista)
        self.vista.append_column(columnaNombre)
        self.vista.append_column(columnaApellido)
        self.vista.append_column(columnaRut)
        
        if (self.num == 6):
            self.vista.append_column(columnaPrevision)
        self.vista.show()      

    def actualizar_atenciones_Treeview(self):		
        for col in self.vista.get_columns():
            self.vista.remove_column(col)
        # Base de datos
        x = Database()
        # Doy valor a la columna
        
        lista = Gtk.ListStore(int, str, str, str, bool)

        # Consulta para obtener los pacientes.
        sql = """
            select id_atencion, fecha as fecha_atencion, 
	    ficha_cliente.nombre as nombre_cliente, hora, estado_atencion from atencion inner join secretaria
	    on secretaria.rut = atencion.secretaria_rut inner join 
	    ficha_cliente on ficha_cliente.rut = atencion.ficha_cliente_rut ;
	    """        
        asd = x.run_select(sql)
        
	# agregar elementos al TreeView

        if asd:
            for r in asd:
                lista.append([r[0], str(r[1]), r[2], str(r[3]), r[4]])
        
        # titulos de la columna
        
        render = Gtk.CellRendererText()

        # columnas
        columnaNombre = Gtk.TreeViewColumn("Número Atención",render,text = 0)
        columnaApellido = Gtk.TreeViewColumn("Fecha",render,text = 1)
        columnaRut = Gtk.TreeViewColumn("Nombre Paciente",render,text = 2)
        columnaPrevision = Gtk.TreeViewColumn("Hora Consulta",render,text = 3)
        columnaEstado = Gtk.TreeViewColumn("Estado",render,text = 4)
        # se agragan datos a las columna
        self.vista.set_model(lista)
        self.vista.append_column(columnaNombre)
        self.vista.append_column(columnaApellido)
        self.vista.append_column(columnaRut)
        self.vista.append_column(columnaPrevision)
        self.vista.append_column(columnaEstado)
        self.vista.show()
        
    def actualizar_pacientes_treeView(self, btn = None):
        for col in self.vista.get_columns():
            self.vista.remove_column(col)
        # Base de datos
        x = Database()
	    # Doy valor a la columna
        lista = Gtk.ListStore(str, str, int, str)
        # Consulta para obtener los pacientes.
        sql = """
	    select ficha_cliente.nombre, apellido, rut, prevision.nombre
	    from ficha_cliente inner join prevision on prevision.id_prevision = ficha_cliente.prevision_id_prevision;
	    """        
        asd = x.run_select(sql)
        
	    # agregar elementos al TreeView

        if asd:
            for r in asd:
                lista.append([r[0], r[1], r[2], r[3]])
        
        # titulos de la columna
        
        render = Gtk.CellRendererText()

        # columnas
        columnaNombre = Gtk.TreeViewColumn("Nombre",render,text = 0)
        columnaApellido = Gtk.TreeViewColumn("Apellido",render,text = 1)
        columnaRut = Gtk.TreeViewColumn("Rut",render,text = 2)
        columnaPrevision = Gtk.TreeViewColumn("Previsión",render,text = 3)

        # se agragan datos a las columna
        self.vista.set_model(lista)
        self.vista.append_column(columnaNombre)
        self.vista.append_column(columnaApellido)
        self.vista.append_column(columnaRut)
        self.vista.append_column(columnaPrevision)
        self.vista.show()

    def actualizar_treeView_prevision_sexo_medicamentos(self):
        for col in self.vista.get_columns():
            self.vista.remove_column(col)
        
	    # Doy valor a la columna
        lista = Gtk.ListStore(str)
        if (self.num == 3): 
            # Base de datos
            x = Database()    
            # Consulta para obtener los pacientes.
            sql = """
               select nombre from prevision;
               """        
            asd = x.run_select(sql)
            if asd:
                for r in asd:
                    lista.append([r[0]])

        elif (self.num == 4):
            # Base de datos
            x = Database()    
            # Consulta para obtener los pacientes.
            sql = """
               select nombre from sexo;
               """        
            asd = x.run_select(sql)
            if asd:
                for r in asd:
                    lista.append([r[0]])
        elif (self.num == 5):
            # Base de datos
            x = Database()    
            # Consulta para obtener los pacientes.
            sql = """
               select nombre from Medicamentos;
               """        
            asd = x.run_select(sql)
            if asd:
                for r in asd:
                    lista.append([r[0]])             			          
        else:
            print ("no hay valor")
        
        render = Gtk.CellRendererText()
        # columnas
        columnaNombre = Gtk.TreeViewColumn("Nombre",render,text = 0)
        # se agragan datos a las columna
        self.vista.set_model(lista)
        self.vista.append_column(columnaNombre)
        self.vista.show() 


class Ventana_inicial:
	
    def __init__(self):
    
        self.builder = Gtk.Builder()
        self.builder.add_from_file("asd/Mipequeñoglade.glade")
        
        self.ventana_inicial = self.builder.get_object("ventana_inicial")
        # botones
        self.boton_salir = self.builder.get_object("boton_salir")
        self.boton_medico = self.builder.get_object("boton_medico")
        self.boton_secretaria = self.builder.get_object("boton_secretaria")
        # eventos
        self.boton_salir.connect("clicked", self.boton_click_salir)  
        self.boton_medico.connect("clicked", self.boton_click_medico) 
        self.boton_secretaria.connect("clicked", self.boton_click_secretaria) 
        
        # label 
        self.labelet = self.builder.get_object("label100")
        self.ventana_inicial.show_all() 
        Gtk.main()

    def boton_click_salir(self, btn = None):
        # cierra ventana.
        self.ventana_inicial.destroy()
        exit()
        
    def boton_click_medico(self, btn = None):
        self.ventana_inicial.destroy()
        k = Inicio_sesion(1)
        
    def boton_click_secretaria(self, btn = None):
        self.ventana_inicial.destroy()
        k = Inicio_sesion(2)
        
# Ventana Inicio Sesión.
class Inicio_sesion:
	
    def __init__(self, parametro):
		
		# obtener valor parametro
		
        self.parametro = parametro
    
        self.builder = Gtk.Builder()
        self.builder.add_from_file("asd/Mipequeñoglade.glade")
        self.win_main = self.builder.get_object("Reconocer_Medico")
        
        # botones
        self.boton_aceptar = self.builder.get_object("aceptar")
        self.boton_cancelar = self.builder.get_object("cancelar")
        self.boton_anadir = self.builder.get_object("añadir")
        
        # eventos
        self.boton_aceptar.connect("clicked", self.boton_click_aceptar)  
        self.boton_cancelar.connect("clicked", self.boton_click_cancelar) 
        
		# GTK ENTRY
        self.rut = self.builder.get_object("rut")
        self.clave = self.builder.get_object("clave")
        
        # label
        self.label = self.builder.get_object("label1")
        
        self.win_main.show_all() 
        Gtk.main()

    def boton_click_cancelar(self, btn = None):
        # cierra ventana.
        self.win_main.destroy()
        z = Ventana_inicial()
        self.parametro = 0
        
    def boton_click_aceptar(self, btn = None):
        # Obtengo valor de la entrada	
        x = self.rut.get_text()  # rut
        z = self.clave.get_text()  # clave 
        
        # Vacío campo clave
        self.clave.set_text("")
        
        # Medico
        if self.parametro == 1:    			
            try:
                # valido el rut			
                aux = int(x)
                # validar si existe el medico
                db = Database()
                sql = """
                    select * from Medico where rut = %(rut)s;
                    """        
                # asd = tupla con toda la primera busqueda
                asd = db.run_select_filter(sql, {'rut': x})
                try:
                    aux = asd[0]	
                    asd = aux[0]

                    nombre = aux[1]
                    apellido = aux[2]
                
					# asd es el INT del rut.
					
					# Validación clave
					
                    sql = """
                        select * from Medico where clave = %(clave)s;
                        """
                    clave = db.run_select_filter(sql, {'clave': z})
					
                    aux = clave[0]
                    clave = aux
                    try:
						
                        k = Clientes(asd, nombre, apellido) # del Medico
                        aux = "   "
                        self.label.set_text(aux)
                        self.win_main.destroy()
                        
                    except IndexError: 
                        aux = "Clave No encontrado."
                        self.label.set_text(aux)    
                        				               
                except IndexError: 
                    aux = "No encontrado."
                    self.label.set_text(aux)
                
            except ValueError:
                aux = "Inválido."
                self.label.set_text(aux)
                

        if self.parametro == 2:    			
            try:
                # valido el rut			
                aux = int(x)
                # validar si existe el medico
                db = Database()
                sql = """
                    select * from secretaria where rut = %(rut)s;
                    """        
                # asd = tupla con toda la primera busqueda
                asd = db.run_select_filter(sql, {'rut': x})
                try:
                    aux = asd[0]	
                    asd = aux[0]
                    nombre_secre = aux[1]
                    apellido_secre = aux[2]
                    sql = """
                        select * from secretaria where clave = %(clave)s;
                        """
                    clave = db.run_select_filter(sql, {'clave': z})
					
                    aux = clave[0]
                    clave = aux
                    try:
                        num = 0
                        m = ventana_secretaria(num)

                        aux = "   "
                        self.label.set_text(aux)
                        self.win_main.destroy()
                        
                    except IndexError: 
                        aux = "Clave No encontrado."
                        self.label.set_text(aux)    
                
                except IndexError: 
                    aux = "Rut No encontrado."
                    self.label.set_text(aux)
                
            except ValueError:
                aux = "Rut Inválido."
                self.label.set_text(aux)
                
if __name__ == '__main__':
    app = Ventana_inicial()
