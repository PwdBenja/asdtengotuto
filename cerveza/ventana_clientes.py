#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import gi
import time
import random

gi.require_version('Gtk', '3.0')

from random import randint
from gi.repository import Gtk
from BaseDatos import Database

# Función retorna el valor seleccionado del combo box
def seleccion_combo_box(combo):
    tree_iter = combo.get_active_iter()
    if tree_iter is not None:
        model = combo.get_model()
        seleccion = model[tree_iter][0]
    return seleccion
    
    
    
class ficha:
    
    def __init__(self, aux):	
		
        self.builder = Gtk.Builder()
        self.builder.add_from_file("asd/FakeTreeView.glade")
        
        # ventana
        self.Ficha = self.builder.get_object("dakiti")
        
        # Botones de administrar.
        self.boton_cerrar = self.builder.get_object("cerraar_truee1")
        
        # Eventos
        self.boton_cerrar.connect("clicked", self.boton_click_cerrar)  

        self.label_nombre = self.builder.get_object("label_apellido1")
		
        self.label_diagnostico = self.builder.get_object("label_diagnostico")
        self.label_tratamiento = self.builder.get_object("label_tratamiento")
        self.label_duracion = self.builder.get_object("label_duracion")
        self.label_enfermedad = self.builder.get_object("label_enfermedad")
        self.label_medicamento = self.builder.get_object("label_medicamento")
        
        x = Database()
        sql = """ select id_diagnostico, ficha_cliente_rut, ficha_cliente.nombre, ficha_cliente.apellido, enfermedad.nombre , 
        tratamiento.duracion as duracion, tratamiento.descripcion, Medicamentos.nombre from diagnostico inner join ficha_cliente 
        on ficha_cliente.rut = diagnostico.ficha_cliente_rut inner join enfermedad on enfermedad.id_enfermedad  = diagnostico.enfermedad_id_enfermedad inner 
        join diagnostico_has_tratamiento on diagnostico_has_tratamiento.diagnostico_id_diagnostico = diagnostico.id_diagnostico inner join tratamiento on 
        tratamiento.id_tratamiento = diagnostico_has_tratamiento.tratamiento_id_tratamiento inner join tratamiento_has_Medicamentos on
        tratamiento_has_Medicamentos.tratamiento_id_tratamiento = tratamiento.id_tratamiento inner join Medicamentos on 
        Medicamentos.id_medicamento = tratamiento_has_Medicamentos.Medicamentos_id_medicamento where ficha_cliente_rut = %(rut)s ;
         """ 
        asd = x.run_select_filter(sql, {'rut': aux})
        
        
        self.label_nombre.set_text(asd[0][2])
        self.label_tratamiento.set_text(asd[0][6])
        self.label_duracion.set_text(str(asd[0][5]))
        self.label_enfermedad.set_text(asd[0][4])
        self.label_medicamento.set_text(asd[0][7])
        
        
        self.Ficha.show_all()    
            
    def boton_click_cerrar(self, btn = None):
        self.Ficha.destroy()    
    
class view_ficha:
    
    def __init__(self):	
		
        self.builder = Gtk.Builder()
        self.builder.add_from_file("asd/FakeTreeView.glade")
        
        # ventana
        self.viewFicha = self.builder.get_object("ventana_ver_fichas")
        
        # Botones de administrar.

        self.boton_anadir = self.builder.get_object("aceptar_ficha")		

        self.boton_cerrar = self.builder.get_object("salir_ficha")
        
        # Eventos

        self.boton_anadir.connect("clicked", self.boton_click_aceptar)  

        self.boton_cerrar.connect("clicked", self.boton_click_cerrar)  

        # TreeView
        self.vista = self.builder.get_object("vista_ficha")
        
        self.zapato = True;
        
        self.actualizar_Fichas_treeView()
        self.viewFicha.show_all()    
        
    def actualizar_Fichas_treeView(self):   
        for col in self.vista.get_columns():
            self.vista.remove_column(col)
            
        # Base de datos
        x = Database()
	    # Doy valor a la columna
        lista = Gtk.ListStore(int, str, str)
        
        # Consulta para obtener las enfermedades.
        sql = """
	    select ficha_cliente_rut, ficha_cliente.nombre, ficha_cliente.apellido  from diagnostico inner join 
	    ficha_cliente on ficha_cliente.rut = diagnostico.ficha_cliente_rut group by ficha_cliente_rut asc;

	    """        
        asd = x.run_select(sql)
        
	    # agregar elementos al TreeView

        if asd:
            for r in asd:
                lista.append([r[0], r[1], r[2]])
        
        # titulos de la columna
        
        render = Gtk.CellRendererText()

        # columnas
        columnaRut = Gtk.TreeViewColumn("Rut",render,text = 0)
        columnaNombre = Gtk.TreeViewColumn("Nombre",render,text = 1)
        columnaApellido = Gtk.TreeViewColumn("Apellido",render,text = 2)
        
        # se agragan datos a las columna
        self.vista.set_model(lista)
        self.vista.append_column(columnaRut)
        self.vista.append_column(columnaNombre)
        self.vista.append_column(columnaApellido)
        self.vista.show() 
        
    def boton_click_aceptar(self, btn = None):
        seleccion = self.vista.get_selection()
        model, treeiter = seleccion.get_selected()
        aux = model[treeiter][0]
        asdasd = ficha(aux)
        
    def boton_click_cerrar(self, btn = None):
        self.viewFicha.destroy()	    

class AtencionTrue:

    def __init__(self, rut_medico, id_atencion, apellido_medico, nombre_medico):	
        # valores
        self.rut_medico = rut_medico
        self.id_atencion = id_atencion
        self.apellido_medico = apellido_medico
        self.nombre_medico = nombre_medico
        self.builder = Gtk.Builder()
        self.builder.add_from_file("asd/FakeTreeView.glade")
        
        # ventana
        self.viewTrue = self.builder.get_object("dakiti_Atencion_true")
        
        # Botones de administrar.
        self.boton_si = self.builder.get_object("aplicar_True")		
        self.boton_cerrar = self.builder.get_object("cerraar_truee")
        
        # Eventos
        self.boton_si.connect("clicked", self.boton_click_si)  
        self.boton_cerrar.connect("clicked", self.boton_click_cerrar)  
        
        # labels
        self.label_nombre = self.builder.get_object("label_nombre")		
        self.label_apellido = self.builder.get_object("label_apellido")
        self.label_sexo = self.builder.get_object("label_sexo")
        self.label_prevision = self.builder.get_object("label_prevision")
        
        
        # combo
        self.vista = self.builder.get_object("combo_enfermedad")
        self.vista1 = self.builder.get_object("combo_duracion")
        self.vista2 = self.builder.get_object("combo_medicamento")
        
        # entry
        self.diagnostico = self.builder.get_object("entry_diagnostico")
        self.tratamiento = self.builder.get_object("entry_diagnostico")

        self.actualizar_contenido()
        self.viewTrue.show_all()    
        
        
    def boton_click_si(self, btn = None):
        # obtengo valores agrego a tablas.
        
        enfermedad =  seleccion_combo_box(self.vista)          
        
        dias = seleccion_combo_box(self.vista1)
        
        medicamento = seleccion_combo_box(self.vista2)
        
        
        diagnostico_txt = self.diagnostico.get_text()
        tratamiento_txt = self.tratamiento.get_text()
        
        if str(enfermedad) != 'Seleccione' and str(dias) != 'Seleccione': 
            if len(diagnostico_txt) > 5 and len(tratamiento_txt) > 5:
                db = Database()	
                
                # obtener el rut, de la consulta.
                sql = """ select ficha_cliente_rut from atencion where id_atencion = %(id)s; """
                rut_cliente = db.run_select_filter(sql, {'id': self.id_atencion})
                
                # id enfermedad
                sql = """ select id_enfermedad from enfermedad where nombre = %(id)s; """
                enfermedad_id = db.run_select_filter(sql, {'id': enfermedad})
                
                sql = """
                    insert into diagnostico (ficha_cliente_rut, enfermedad_id_enfermedad, ficha_cliente_id_ficha_cliente, descripcion) values
                    (%(rut_cliente)s,%(enfermedad)s, 0, %(descripcion)s) 
                """        
                # SE INSERTA EL DIAGNOSTICO.
                dsa = db.run_sql(sql, {'rut_cliente': rut_cliente[0][0], 'enfermedad': enfermedad_id[0][0], 'descripcion': diagnostico_txt})
 

                sql = """
                    insert into tratamiento (descripcion, duracion) values (%(descripcion)s, %(duracion)s)
                    
                """        
                # SE INSERTA EL tratamiento
                dsa = db.run_sql(sql, {'descripcion': tratamiento_txt, 'duracion': int(dias)})
                
                # vincular el tratamiento su diagnostico.
                
                # obtener los id
                
                sql = """ select id_tratamiento from tratamiento where descripcion = %(id)s; """
                
                id_tratamiento = db.run_select_filter(sql, {'id': tratamiento_txt})
                
                
                sql = """ select id_diagnostico from diagnostico where descripcion = %(id)s; """
                
                id_diagnostico = db.run_select_filter(sql, {'id': diagnostico_txt})
                
                
                # relleno la tabla diagnostico_has_tratamiento
                sql = """
                    insert into diagnostico_has_tratamiento (diagnostico_id_diagnostico, tratamiento_id_tratamiento)
                    values (%(diag)s, %(tra)s)
                    
                """        
                # SE INSERTA EL 
                dsa = db.run_sql(sql, {'diag': id_diagnostico[0][0], 'tra':id_tratamiento[0][0] })
                
                
                # INSERTAR MEDICAMENTO Y VINCULARLO 

                sql = """ select id_medicamento from Medicamentos where nombre = %(id)s; """
                
                id_medicamento = db.run_select_filter(sql, {'id': medicamento})
                
                sql = """ insert into tratamiento_has_Medicamentos (tratamiento_id_tratamiento, Medicamentos_id_medicamento)
                values (%(id_tratamiento)s, %(id_medicamento)s)"""
                
                db.run_sql(sql, {'id_tratamiento':id_tratamiento[0][0], 'id_medicamento': id_medicamento[0][0]})
                
                
                self.viewTrue.destroy()
                asdasda = Clientes(self.rut_medico, self.nombre_medico, self.apellido_medico)
            else:
                print ("warning")				        			
        else:
            print("warning")    			    				

        				           
    def boton_click_cerrar(self, btn = None):
		
        dialog = Gtk.MessageDialog(parent=self.viewTrue, flags=0, message_type=Gtk.MessageType.QUESTION, 
            buttons=Gtk.ButtonsType.YES_NO, text="¡CUIDADO!")
        dialog.format_secondary_text("Usted cerrará la atención del PACIENTE. Volverá a la lista de atenciones.")
         
        response = dialog.run()
            
        if response == Gtk.ResponseType.YES:      
            self.viewTrue.destroy()
            # activar el 2do trigger.   

            # ~ mysql> delimiter $$
            # ~ mysql> CREATE TRIGGER fallido BEFORE UPDATE ON atencion
            # ~ -> FOR EACH ROW 
            # ~ -> BEGIN 
            # ~ -> IF NEW.Medico_rut is NULL then
            # ~ -> set NEW.estado_atencion = False;
            # ~ -> END IF;
            # ~ -> END $$
            sql = """ update atencion set Medico_rut = NULL where id_atencion = %(id)s;
            """
            db = Database()	
            db.run_sql(sql, {'id': self.id_atencion})
            asdasda = Clientes(self.rut_medico, self.nombre_medico, self.apellido_medico)
        dialog.destroy()        
        
        
        
    def actualizar_contenido(self, btn = None):
        # los label obtienen valores.
        db = Database()	
        
        sql = """ select ficha_cliente.nombre, ficha_cliente.apellido, sexo.nombre as sexo, prevision.nombre as prevision
         from atencion inner join ficha_cliente on ficha_cliente.rut = atencion.ficha_cliente_rut inner join
          sexo on sexo.id_sexo = ficha_cliente.sexo_id_sexo inner join prevision on prevision.id_prevision = ficha_cliente.prevision_id_prevision 
          where  id_atencion = %(id)s ; """

        asd = db.run_select_filter(sql, {'id': self.id_atencion})
        
        if asd:
            self.label_nombre.set_text(asd[0][0])
            self.label_apellido.set_text(asd[0][1])
            self.label_sexo.set_text(asd[0][2])
            self.label_prevision.set_text(asd[0][3])
                
        renderer = Gtk.CellRendererText()
        self.vista.pack_start(renderer, True)
        self.vista.add_attribute(renderer, "text", 0)
        
        sql = """
            select nombre from enfermedad;
            """                    
        asd = db.run_select(sql)
       
        self.store = Gtk.ListStore(str)
        self.store.append(["Seleccione"])
        if asd:
            for r in asd:
                self.store.append([r[0]])
      
        self.vista.set_model(self.store)
        self.vista.set_active(0)
        
        # box dias
        self.vista1.pack_start(renderer, True)
        self.vista1.add_attribute(renderer, "text", 0)
        
        self.store1 = Gtk.ListStore(str)
        
        self.store1.append(["Seleccione"])
        self.store1.append(["8"])     			    
        self.store1.append(["9"])     			    
        self.store1.append(["10"])     			    
        self.store1.append(["11"])     			    
        self.store1.append(["12"])     			    
        self.store1.append(["13"])     			    
        self.store1.append(["14"])     			    
        self.store1.append(["15"])     			    
        self.store1.append(["16"])     			    
        self.store1.append(["17"])     			    
        self.store1.append(["18"])     			    
        self.store1.append(["19"])     			    
        self.store1.append(["20"])     			    
        self.store1.append(["21"])     			    
        self.store1.append(["22"])     			    
        self.store1.append(["23"])     			    

        self.vista1.set_model(self.store1)
        self.vista1.set_active(0)
        
        # box medicamentos
        self.vista2.pack_start(renderer, True)
        self.vista2.add_attribute(renderer, "text", 0)
        
        sql = """
            select nombre from Medicamentos;
            """                    
        asd = db.run_select(sql)
       
        self.store2 = Gtk.ListStore(str)
        self.store2.append(["Seleccione"])
        if asd:
            for r in asd:
                self.store2.append([r[0]])
      
        self.vista2.set_model(self.store2)
        self.vista2.set_active(0)             

class EnfermedadAgregar:

    def __init__(self):	
		
        self.builder = Gtk.Builder()
        self.builder.add_from_file("asd/FakeTreeView.glade")
        
        # ventana
        self.enfermedad_agregar = self.builder.get_object("enfermedad_agregar")
        
        # Botones de administrar.
        self.boton_anadir = self.builder.get_object("ediiiii1")
        self.boton_cerrar = self.builder.get_object("cerrar_editar1")
        # Eventos
        
        self.boton_anadir.connect("clicked", self.boton_click_anadir)  
        
        self.boton_cerrar.connect("clicked", self.boton_click_cerrar)  
        
        self.entrada = self.builder.get_object("entry_edditar1")
        self.enfermedad_agregar.show_all()


    def boton_click_anadir(self, btn = None):
        enfermedad = self.entrada.get_text()
        
        if len(enfermedad) > 3:
            db = Database()	
            sql = """
	            insert into enfermedad (nombre) values
	            (%(nombre)s)
	            """      
            db.run_sql(sql, {'nombre': enfermedad})
        else:
            print ("cueck, largo de la enfermedad no valido")			             				    				
        
        self.enfermedad_agregar.destroy()
        x = ViewEnfermedades()
  
    def boton_click_cerrar(self, btn = None):
        self.enfermedad_agregar.destroy()



class EnfermedadEditar:

    def __init__(self, aux):	
		
        self.builder = Gtk.Builder()
        self.builder.add_from_file("asd/FakeTreeView.glade")
        
        # ventana
        self.enfermedad_editar = self.builder.get_object("enfermedad_editar")
        
        # Botones de administrar.
        self.boton_editar = self.builder.get_object("ediiiii")
        self.boton_cerrar = self.builder.get_object("cerrar_editar")
        # Eventos
        self.boton_editar.connect("clicked", self.boton_click_editar)  
        self.boton_cerrar.connect("clicked", self.boton_click_cerrar)  
        db = Database()	
        sql = """
	        select nombre, id_enfermedad from enfermedad  where nombre = (%(nombre)s);
	        """      
        self.x = db.run_select_filter(sql, {'nombre': aux})
            
        self.entrada = self.builder.get_object("entry_edditar")
        self.entrada.set_text(self.x[0][0])
        self.enfermedad_editar.show_all()

    def boton_click_editar(self, btn = None):
        enfermedad = self.entrada.get_text()
        
        if len(enfermedad) > 3:
            db = Database()	
            sql = """
                  update enfermedad set nombre = %(nombre)s where id_enfermedad = %(nombre_1)s
	            """      
            db.run_sql(sql, {'nombre': enfermedad, 'nombre_1': self.x[0][1]})
        else:
            print ("cueck, largo de la enfermedad no valido")			             				    				
        
        self.enfermedad_editar.destroy()
        uwu = ViewEnfermedades()
  
    def boton_click_cerrar(self, btn = None):
        self.enfermedad_editar.destroy()
        
        
class ViewEnfermedades:

    def __init__(self):	
		
        self.builder = Gtk.Builder()
        self.builder.add_from_file("asd/FakeTreeView.glade")
        
        # ventana
        self.viewEnfermedades = self.builder.get_object("ventana_enfermedad")
        
        # Botones de administrar.
        self.boton_quitar = self.builder.get_object("quitar_enfermedad")		
        self.boton_anadir = self.builder.get_object("anadir_enfermedad")		
        self.boton_editar = self.builder.get_object("editar_enfermedad")
        self.boton_cerrar = self.builder.get_object("cerrar_enfermedad")
        # Eventos
        self.boton_quitar.connect("clicked", self.boton_click_quitar)  
        self.boton_anadir.connect("clicked", self.boton_click_anadir)  
        self.boton_editar.connect("clicked", self.boton_click_editar)  
        self.boton_cerrar.connect("clicked", self.boton_click_cerrar)  
        # TreeView
        self.vista = self.builder.get_object("view_enfermedades")
        
        self.zapato = True;
        
        self.actualizar_enfermedades_treeView()
        self.viewEnfermedades.show_all()    
        
        
    def boton_click_editar(self, btn = None):
        seleccion = self.vista.get_selection()
        model, treeiter = seleccion.get_selected()
        aux = model[treeiter][0]
        aux = str(aux)
        uwu = EnfermedadEditar(aux)
        self.viewEnfermedades.destroy()
        				           
    def boton_click_quitar(self, btn = None):
        seleccion = self.vista.get_selection()
        model, treeiter = seleccion.get_selected()
        aux = model[treeiter][0]
        aux = str(aux)
        x = Database()
       
        if treeiter is not None:
            dialog = Gtk.MessageDialog(parent=self.viewEnfermedades, flags=0, message_type=Gtk.MessageType.QUESTION, 
                buttons=Gtk.ButtonsType.YES_NO, text="Se eliminara a " + model[treeiter][0])
            dialog.format_secondary_text("Esta acción no se puede revertir.")
            
            response = dialog.run()
            
            if response == Gtk.ResponseType.YES:
                sql = """
                    delete from enfermedad where nombre = %(nombre)s
                """					
                x.run_sql(sql, {'nombre': aux})
                self.actualizar_enfermedades_treeView()
                self.viewEnfermedades.show()
            dialog.destroy()
        
        
    def boton_click_anadir(self, btn = None):
        uwu = EnfermedadAgregar()        
        self.viewEnfermedades.destroy()    
  
    def boton_click_cerrar(self, btn = None):
        self.viewEnfermedades.destroy()

    def actualizar_enfermedades_treeView(self, btn = None):
        for col in self.vista.get_columns():
            self.vista.remove_column(col)
            
        # Base de datos
        x = Database()
	    # Doy valor a la columna
        lista = Gtk.ListStore(str)
        
        # Consulta para obtener las enfermedades.
        sql = """
	    select nombre from enfermedad;
	    """        
        asd = x.run_select(sql)
        
	    # agregar elementos al TreeView

        if asd:
            for r in asd:
                lista.append([r[0]])
        
        # titulos de la columna
        
        render = Gtk.CellRendererText()

        # columnas
        columnaNombre = Gtk.TreeViewColumn("Nombre",render,text = 0)
        
        # se agragan datos a las columna
        self.vista.set_model(lista)
        self.vista.append_column(columnaNombre)
        self.vista.show()

# Ventana Clientes.
class Clientes:
    
    def __init__(self, rut, nombre, apellido):
        
        #igualo parametro
        self.rut = str(rut)
        self.nombre = nombre
        self.apellido = apellido
        self.id = 0
        self.builder = Gtk.Builder()
        self.builder.add_from_file("asd/Mipequeñoglade.glade")
        self.ventana_cliente = self.builder.get_object("clientes")
        self.zapato = True
        # botones
        self.boton_salir = self.builder.get_object("salir")
        self.boton_enfermedades = self.builder.get_object("admin_enfermedad") # administrar enfermedades
        self.boton_atender = self.builder.get_object("atender")
        
        self.boton_revisar = self.builder.get_object("revisar")
        
        # clicked evento. 
        self.boton_salir.connect("clicked", self.boton_click_salir)  
        self.boton_enfermedades.connect("clicked", self.boton_click_enfermedad)  
        self.boton_atender.connect("clicked", self.boton_click_atender)  
        self.boton_revisar.connect("clicked", self.boton_click_revisar)  
        
        
        # label
        self.label_rut = self.builder.get_object("label_rut")
        self.label_nombre = self.builder.get_object("label_nombre")
        self.label_rut.set_text(self.rut)
        self.label_nombre.set_text(self.nombre + " " + self.apellido)

        # TreeView 
        self.vista = self.builder.get_object("treeviewClientes")
        self.actualizar_treeView_atenciones_paciente()
        self.ventana_cliente.show_all()

    def boton_click_salir(self, btn = None):
        # se cierra ventana.
        self.ventana_cliente.destroy()
        exit()
        
    def boton_click_enfermedad(self, btn = None):
        uwu = ViewEnfermedades()

    def boton_click_atender(self, btn = None):
        ###  trigger

        # ~ sql = """
            # ~ CREATE TRIGGER mensaje BEFORE UPDATE ON atencion FOR EACH ROW BEGIN IF NEW.Medico_rut IS NOT NULL 
            # ~ then set NEW.estado_atencion = True; end if; end$$
         # ~ """                    

        seleccion = self.vista.get_selection()
        model, treeiter = seleccion.get_selected()
        aux = model[treeiter][7]
              
        if treeiter is not None:
            dialog = Gtk.MessageDialog(parent=self.ventana_cliente, flags=0, message_type=Gtk.MessageType.QUESTION, 
                buttons=Gtk.ButtonsType.YES_NO, text="Atenderá a " + model[treeiter][0])
            dialog.format_secondary_text("Recuerde Tener ingresado: MEDICAMENTOS Y ENFERMEDADES PARA UNA CORRECTA ATENCIÓN.")

            response = dialog.run()
            
            if response == Gtk.ResponseType.YES:      
                db = Database()	
        
                sql = """ update atencion set Medico_rut = %(rut_medico)s where id_atencion = %(id)s; """

                db.run_sql(sql, {'rut_medico': self.rut, 'id': aux})
                self.actualizar_treeView_atenciones_paciente()
                self.ventana_cliente.show_all()

                #### LLAMAMOS VENTANA PARA ATENDER AL PACIENTE.
                # ~ SELECT count(*) FROM tabla
                poom = AtencionTrue(self.rut, self.id, self.apellido, self.nombre)
                self.actualizar_treeView_atenciones_paciente()
                self.ventana_cliente.destroy()
                
            dialog.destroy()	 
            
    def boton_click_revisar(self, btn = None):
        xdxd = view_ficha()
		
		
		
    def actualizar_treeView_atenciones_paciente(self):
        for col in self.vista.get_columns():
            self.vista.remove_column(col)
            		
        # Doy valor a la columna
        self.lista = Gtk.ListStore(str, str, int, str, str, str, str, int)
        # Datos que se imprimen
        
        ## VISTA PARA CADA CONVENIO DE MEDICO
        x = Database()
 
        # obtengo el convenio del doctor
        sql = """
            select prevision_id_prevision from Medico where rut = %(rut)s;
        """ 
        asd = x.run_select_filter(sql, {'rut': int(self.rut)}) 
        
        convenio_doctor = asd[0][0]

        sql = """ select * from vista_paciente%(x)s  """
        
        asd = x.run_select_filter(sql, {'x':convenio_doctor})
        
        # agregar elementos al TreeView

        if asd:
            for r in asd:
                self.lista.append([r[0], r[1], r[2], r[3], r[4], str(r[5]), str(r[6]), r[7]])
            self.id = r[7]
        # titulos de la columna
        
        render = Gtk.CellRendererText()
        
        # columnas
        columnaNombre = Gtk.TreeViewColumn("Nombre",render,text = 0)
        columnaApellido = Gtk.TreeViewColumn("Apellido",render,text = 1)
        columnaRut = Gtk.TreeViewColumn("Rut",render,text = 2)
        columnaPrevision = Gtk.TreeViewColumn("Previsión",render,text = 3)
        columnaSexo = Gtk.TreeViewColumn("sexo",render,text = 4)
        columnaHora = Gtk.TreeViewColumn("Hora", render, text = 5)
        columnaFecha = Gtk.TreeViewColumn("Fecha", render, text = 6)
        columnaid = Gtk.TreeViewColumn("Id Atención", render, text = 7)
        # se agragan datos a las columna
        self.vista.set_model(self.lista)
        self.vista.append_column(columnaNombre)
        self.vista.append_column(columnaApellido)
        self.vista.append_column(columnaRut)
        self.vista.append_column(columnaPrevision)
        self.vista.append_column(columnaSexo)
        self.vista.append_column(columnaHora)
        self.vista.append_column(columnaFecha)
        self.vista.append_column(columnaid)
        self.vista.show()
        
